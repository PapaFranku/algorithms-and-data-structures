﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Algorithms_And_Data_Structures.Search;

namespace Algorithms_And_Data_Structures.TestingFramework
{
    public class PerformanceFramework
    {
        public int TestDataSize { get; set; }
        public int MaxIntegerValue { get; set; }
        public int MinIntegerValue { get; set; }
        public int[] TestData { get; set; }
        public List<ITestSubject> TestSubjects { get; set; }

        private Stopwatch stopwatch;
        private RNG rng;

        public PerformanceFramework(int minIntegerValue, int maxIntegerValue, int testDataSize, List<ITestSubject> testSubjects)
        {
            stopwatch = new Stopwatch();
            rng = new RNG();

            MinIntegerValue = minIntegerValue;
            MaxIntegerValue = maxIntegerValue;
            TestData = new int[testDataSize];
            TestDataSize = testDataSize;
            TestSubjects = testSubjects;
        }

        public PerformanceFramework(int minIntegerValue, int maxIntegerValue, int testDataSize) 
            : this(minIntegerValue, maxIntegerValue, testDataSize, new List<ITestSubject>()) { }

        public void Setup()
        {
            TestData = rng.GenerateNumbersBetween(MinIntegerValue, MaxIntegerValue, TestDataSize);
        }

        public IEnumerable<Test> RunTests(int timesToRunTests = 1)
        {
            if (timesToRunTests < 1)
                throw new ArgumentOutOfRangeException("Parameter " + nameof(timesToRunTests) + "cannot be less than 1");

            if (TestData.Length == 0)
                Setup();

            foreach(ITestSubject subject in TestSubjects)
            {
                Test test = new Test(subject.GetType());
                stopwatch.Start();
                subject.Test(TestData);
                stopwatch.Stop();
                test.ElapsedTicks = stopwatch.ElapsedTicks;
                test.Time = stopwatch.ElapsedMilliseconds;
                stopwatch.Reset();

                yield return test;
            }
        }
    }
}
