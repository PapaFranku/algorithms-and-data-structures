﻿using System;
using System.Security.Cryptography;

namespace Algorithms_And_Data_Structures.TestingFramework
{
    public class RNG
    {
        private readonly RandomNumberGenerator generator;

        public RNG()
        {
            generator = RandomNumberGenerator.Create();
        }

        public int GenerateNumberBetween(int minimumValue, int maximumValue)
        {
            byte[] randomNumber = new byte[1];
            generator.GetBytes(randomNumber);

            double asciiValueOfRandomCharacter = Convert.ToDouble(randomNumber[0]);
            double multiplier = Math.Max(0, (asciiValueOfRandomCharacter / 255d) - 0.00000000001d);

            int range = maximumValue - minimumValue + 1;
            double randomValueInRange = Math.Floor(multiplier * range);

            return (int)(minimumValue + randomValueInRange);
        }

        public int[] GenerateNumbersBetween(int minimumValue, int maximumValue, int amountOfInts)
        {
            if (amountOfInts < 1)
                throw new ArgumentOutOfRangeException("Parameter " + nameof(amountOfInts) + "cannot be less than 1");

            int[] numbers = new int[amountOfInts];

            for (int i = amountOfInts - 1; i >= 0; i--)
            {
                byte[] randomNumber = new byte[1];
                generator.GetBytes(randomNumber);

                double asciiValueOfRandomCharacter = Convert.ToDouble(randomNumber[0]);
                double multiplier = Math.Max(0, (asciiValueOfRandomCharacter / 255d) - 0.00000000001d);

                int range = maximumValue - minimumValue + 1;
                double randomValueInRange = Math.Floor(multiplier * range);

                numbers[i] = (int)(minimumValue + randomValueInRange);
            }

            return numbers;
        }
    }
}
