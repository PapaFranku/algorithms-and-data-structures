﻿using System;

namespace Algorithms_And_Data_Structures.TestingFramework
{
    public class Test
    {
        public Type Subject { get; set; }
        public long Time { get; set; }
        public long ElapsedTicks { get; set; }

        public Test(Type type)
        {
            Subject = type;
        }
    }
}
