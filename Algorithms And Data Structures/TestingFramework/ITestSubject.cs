﻿namespace Algorithms_And_Data_Structures.TestingFramework
{
    public interface ITestSubject
    {
        void Test(int[] data);
    }
}
