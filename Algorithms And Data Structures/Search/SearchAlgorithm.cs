﻿using System;
using Algorithms_And_Data_Structures.TestingFramework;

namespace Algorithms_And_Data_Structures.Search
{
    public abstract class SearchAlgorithm : ITestSubject
    {
        public abstract bool Find(int[] array, ref int key);

        public void Test(int[] data)
        {
            RNG rng = new RNG();
            int key = rng.GenerateNumberBetween(1, 10000);

            Find(data, ref key);
        }
    }
}
