﻿using System;

namespace Algorithms_And_Data_Structures.Search
{
    public class BinarySearch : SearchAlgorithm
    {
        public override bool Find(int[] array, ref int key)
        {
            if (array is null)
                throw new ArgumentNullException("The parameter " + nameof(array) + " cannot be null!");

            if (array.Length <= 0)
                throw new ArgumentOutOfRangeException("The parameter " + nameof(array) + "is empty!");

            int start = 0;
            int end = array.Length - 1;

            while(start <= end)
            {
                int mid = (start + end) / 2;

                if (array[mid] == key)
                    return true;
                else if (array[mid] < key)
                    start = mid + 1;
                else
                    end = mid - 1;
            }

            return false;
        }
    }
}
