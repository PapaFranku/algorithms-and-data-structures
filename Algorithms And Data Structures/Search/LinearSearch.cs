﻿using System;
using System.Collections.Generic;

namespace Algorithms_And_Data_Structures.Search
{
    public class LinearSearch : SearchAlgorithm
    {
        public override bool Find(int[] array, ref int key)
        {
            if (array is null)
                throw new ArgumentNullException("The parameter " + nameof(array) + " cannot be null!");

            if (array.Length <= 0)
                throw new ArgumentOutOfRangeException("The parameter " + nameof(array) + "is empty!");

            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == key)
                    return true;
            }

            return false;
        }
    }
}
