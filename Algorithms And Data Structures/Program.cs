﻿using System;
using System.Collections.Generic;
using Algorithms_And_Data_Structures.Search;
using Algorithms_And_Data_Structures.TestingFramework;

namespace Algorithms_And_Data_Structures
{
    class Program
    {
        static void Main(string[] args)
        {
            List<ITestSubject> searchAlgorithms = new List<ITestSubject>() { new LinearSearch(), new BinarySearch() };
            PerformanceFramework framework = new PerformanceFramework(1, 10000, 99999999, searchAlgorithms);

            foreach(var res in framework.RunTests())
            {
                Console.WriteLine(res.Subject.Name + ":\n" + "Ticks: " + res.ElapsedTicks + "\n" + "Milliseconds: " + res.Time + "ms\n");
            }

            Console.ReadLine();
        }
    }
}