﻿using System;

namespace Tic_Tac_Toe
{
    class Program
    {
        static void Main(string[] args)
        {
            bool newGame = false;

            Console.WriteLine("Tic-Tac-Toe");

            do
            {
                int size = 3;

                do
                {
                    Console.Write("Specify the board size (min - 3, max - 26): ");
                    bool parse = int.TryParse(Console.ReadLine(), out size) && size > 2 && size < 27;

                    if (parse)
                        break;
                    else
                        Console.WriteLine("Invalid input!");
                }
                while (true);

                Console.WriteLine("\nInput the field you wish to place your cross/circle in (eg.'A1').\nIf you wish to correct your move, type 'undo'.\n");

                try
                {
                    GameManager manager = new GameManager(new Board(size));

                    while (!manager.GameEnded)
                        manager.NextMove();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                do
                {
                    Console.Write("Restart? (Y/N): ");

                    string input = Console.ReadLine().ToUpper();

                    if (input == "Y")
                    {
                        newGame = true;
                        break;
                    }
                    else if (input == "N")
                    {
                        newGame = false;
                        break;
                    }
                    else
                        Console.WriteLine("Invalid input!");
                }
                while (true);
            }
            while (newGame);
        }
    }
}
