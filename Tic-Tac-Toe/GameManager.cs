﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Tic_Tac_Toe
{
    public class GameManager
    {
        public bool GameEnded { get; private set; }

        private Stack<(int number, int letterIndex)> movesList;
        private Board board;
        private bool isXTurn = true;

        public GameManager(Board board)
        {
            if (board is null)
                throw new ArgumentNullException(nameof(board) + "is null.");

            this.board = board;
            movesList = new Stack<(int number, int letterIndex)>();
            GameEnded = false;
        }

        public void NextMove()
        {
            int formType;

            if (isXTurn)
            {
                formType = 0;
                Console.Write("\n'X' turn: ");
            }
            else
            {
                formType = 1;
                Console.Write("\n'O' turn: ");
            }

            string input = Console.ReadLine().ToUpper();

            if(input == "UNDO")
            {
                if (movesList.Count == 0)
                {
                    board.Draw();
                    return;
                }

                var lastMove = movesList.Pop();

                board.Undo(lastMove.number, lastMove.letterIndex);
                board.Draw();

                isXTurn = !isXTurn;
                return;
            }

            try
            {
                var cell = getCell(input);

                if(board.WriteToBoard(cell.number, cell.letterIndex, formType))
                {
                    board.Draw();
                    movesList.Push(cell);
                }
                else
                {
                    Console.WriteLine("Invalid move!");
                    return;
                }
            }
            catch (ArgumentException e)
            {
                Console.WriteLine("Invalid input!");
                return;
            }

            GameEnded = checkGameEnded(out bool boardIsFull);

            if(GameEnded)
            {
                if (boardIsFull)
                    Console.WriteLine("\nTie!");
                else if (isXTurn)
                    Console.WriteLine("\nPlayer 'X' wins!");
                else
                    Console.WriteLine("\nPlayer '0' wins!");
            }

            isXTurn = !isXTurn;
        }

        private (int number, int letterIndex) getCell(string input)
        {
            if (input.Length < 2 || !verifyInput(input))
                throw new ArgumentException();

            if (!int.TryParse(input.Remove(0, 1).ToString(), out int number))
                throw new ArgumentException();

            number--; //Make it 0 indexed

            if (number < 0 || number > board.BoardSize - 1)
                throw new ArgumentException();          

            int letterIndex = -1;

            for (int i = 0; i < board.BoardSize; i++)
            {
                if (board.Alphabet[i] == input[0])
                {
                    letterIndex = i;
                    break;
                }
            }

            if (letterIndex == -1)
                throw new ArgumentException();

            return (number: number, letterIndex: letterIndex);
        }

        private bool checkGameEnded(out bool isFull)
        {
            isFull = board.CheckIsFull();
            return isFull || board.CheckColumns() || board.CheckRows() || board.CheckDiagonals();
        }

        private bool verifyInput(string input)
        {
            if (!char.IsLetter(input[0]))
                return false;

            for(int i = 1; i < input.Length; i++)
            {
                if (!char.IsDigit(input[i]))
                    return false;
            }

            return true;
        }
    }
}
