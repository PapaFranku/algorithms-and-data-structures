﻿using System;
using System.Text;

namespace Tic_Tac_Toe
{
    public class Board
    {
        public int BoardSize { get; private set; }
        public char[] Alphabet { get; } = new char[]
        {
            'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'
        };

        private int[][] board;

        public Board(int size)
        {
            if (size < 2 || size > 27)
                throw new ArgumentOutOfRangeException("The board size minimum is 3 and maximum is 26!");

            BoardSize = size;
            board = new int[size][];

            for (int i = 0; i < BoardSize; i++)
            {
                board[i] = new int[BoardSize];

                for (int j = 0; j < BoardSize; j++)
                    board[i][j] = -1;
            }

            Draw();
        }

        public void Draw()
        {
            StringBuilder builder = new StringBuilder();

            Console.Write("  ");

            for (int i = 0; i < BoardSize; i++)
            {
                builder.Append(" " + Alphabet[i]);
            }

            Console.WriteLine(builder.ToString());
            builder.Clear();

            for (int i = 0; i < BoardSize; i++)
            {
                if(i < 9)
                    builder.Append(' ' + (i + 1).ToString() + '|');
                else
                    builder.Append((i + 1).ToString() + '|');

                for (int j = 0; j < BoardSize; j++)
                    {
                        switch (board[i][j])
                        {
                            case -1:
                                builder.Append(" |");
                                break;
                            case 0:
                                builder.Append("X|");
                                break;
                            case 1:
                                builder.Append("O|");
                                break;
                            default:
                                throw new ArgumentException("Invalid field in board!");
                        }
                    }

                Console.WriteLine(builder.ToString());
                builder.Clear();
            }
        }

        public void Undo(int number, int letterIndex)
        {
            board[number][letterIndex] = -1;
        }

        public bool WriteToBoard(int number, int letterIndex, int formType)
        {
            if (board[number][letterIndex] == -1)
            {
                board[number][letterIndex] = formType;
                return true;
            }

            return false;
        }

        public bool CheckRows()
        {
            for (int i = 0; i < BoardSize; i++)
            {
                int startingNumber = board[i][0];

                if (startingNumber == -1)
                    continue;

                bool hasSameTypesOnRow = true;

                for (int j = 0; j < BoardSize; j++)
                {
                    if (board[i][j] == -1 || board[i][j] != startingNumber)
                    {
                        hasSameTypesOnRow = false;
                        break;
                    }
                }

                if (hasSameTypesOnRow)
                    return true;
            }

            return false;
        }

        public bool CheckColumns()
        {
            for (int i = 0; i < BoardSize; i++)
            {
                int startingNumber = board[i][i];

                if (startingNumber == -1)
                    continue;

                bool hasSameTypesOnCol = true;

                for (int j = 0; j < BoardSize; j++)
                {
                    if (board[j][i] == -1 || board[j][i] != startingNumber)
                    {
                        hasSameTypesOnCol = false;
                        break;
                    }
                }

                if (hasSameTypesOnCol)
                    return true;
            }

            return false;
        }

        public bool CheckDiagonals()
        {
            return checkLeft() || checkRight();
        }

        public bool CheckIsFull()
        {
            for (int i = 0; i < BoardSize; i++)
            {
                for (int j = 0; j < BoardSize; j++)
                {
                    if (board[i][j] == -1)
                        return false;
                }
            }

            return true;
        }

        private bool checkLeft()
        {
            int startingNumber = board[0][0];

            if (startingNumber == -1)
                return false;

            for(int i = 1; i < BoardSize; i++)
            {
                if (board[i][i] == -1 || board[i][i] != startingNumber)
                    return false;
            }

            return true;
        }

        private bool checkRight()
        {
            int startingNumber = board[0][BoardSize - 1];

            if (startingNumber == -1)
                return false;

            int boardSize = BoardSize - 1; //Make it 0 indexed

            for (int i = boardSize; i > 0; i--)
            {
                if (board[i][boardSize - i] == -1 || board[i][boardSize - i] != startingNumber)
                    return false;
            }

            return true;
        }
    }
}
